Metadata-Version: 1.0
Name: linty
Version: 1.1.10rc80
Summary: The Predikto config static linting utility
Home-page: https://bitbucket.org/predikto/predikto-linty
Author: Will McGinnis
Author-email: wmcginnis@predikto.com
License: private
Description: Linty 1.x
        =====
        A small utility for linting and doing basic static analysis of config
        files.
        
        
        Examples
        =====
        ```python
        import json
        from pprint import pprint
        from linty import lint, get_verbose_configs_with_psycopg2
        
        vc = get_verbose_configs_with_psycopg2(user='postgres', password='redlobster', port=8888)
        
        
        result = lint(config, vc)
        result = json.dumps(result)
        pprint(result)
        ```
        
        
        Development
        =====
        - `pipenv shell`
        - `pipenv run pytest`
        - vendored marshmallow (_might not need this in the future_)
        
        
        
        
        
        
        Linty pre 1.0
        =====
        
        A small utility for linting and doing basic static analysis of config
        files.  To include things like:
        
         * Making sure the configuration is valid
         * Pulling out any I/O operations and warning about datastores effected
         * Estimating resources and parallelism needed
        
        And probably more later on.
        
        Examples
        ========
        
        Get a list of all problems found with the config file:
        
            cf = ConfigurationFile({some config file})
            issues = cf.find_issues(strict=False)
            print(json.dumps(issues, indent=4, sort_keys=True))
            
        Strict toggles whether to consider a warning an issue or not.
        
        
        Roadmap
        =======
        
        In the next release, you will be able optionally pass a central_db 
        interface into the top level linty config object, which will allow for 
        more detailed checking. 
        
         * make sure all kv:FOOBAR terms are valid
         * make sure that all non-optional config parameters by service are present
         * warn of implicit configs by default value in services
         * make sure that all non-optional dag task parameters are present
         * warn of implicit dag task configs by default value
         * check types of config values in services and dag tasks
         * suggest usage of kv: terms where they match up fields in the kv table
        
        
Platform: UNKNOWN
