FROM python:3.7.4 as builder
WORKDIR /opt/build
COPY . .
RUN pip install -r requirements.txt
#RUN pytest -sv unit_test --cov-report term-missing --cov=test_tools

FROM python:3.7.4-alpine3.10 as runtime
COPY --from=builder /root/.cache /root/.cache

WORKDIR /opt/predikto/

COPY requirements.txt setup.py setup.cfg ./

RUN \
 apk add --no-cache postgresql-libs && \
 apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev && \
 python3 -m pip install -r requirements.txt --no-cache-dir && \
 apk --purge del .build-deps

RUN addgroup -S predikto
RUN adduser -S predikto

RUN pip install -r requirements.txt && rm -rf /root/.cache

COPY linty /opt/predikto/linty/
COPY README.md /opt/predikto/
RUN pip install .

RUN chown -R predikto:predikto /opt/predikto/
USER predikto

CMD ["/bin/sh"]

