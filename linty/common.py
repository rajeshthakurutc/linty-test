"""
.. module:: utils
    :platform: Unix, Linux, Windows
    :synopsis: shared utilities for the linty project

.. moduleauthor:: Will McGinnis <will@predikto.com>

:copyright: (c) 2018 Predikto Inc.

"""

import json
import ast


__author__ = 'willmcginnis'


def flatten_dict(data, layers=1, drop_deeper=True):
        """
        takes in a dictionary and will flatten it with level_1.level_2 as the key, for however many levels are
        specified. If specified (true by default), anything deeper than the specified level will be dropped from the
        dictionary outright.

        :param data: the dictionary to be flattened
        :param layers: the number of layers to flatten, default 1
        :param drop_deeper: boolean for whether to drop layers deeper than that which we are flattening
        :return:
        """

        for _ in range(layers):
            data = [(k, v) if not isinstance(v, dict) else [(k + '.' + k2, v2) for k2, v2 in v.items()] for k, v in data.items()]
            data = [item for sublist in data for item in sublist if isinstance(sublist, list)] + [y for y in data if not isinstance(y, list)]
            data = dict(data)
        if drop_deeper:
            data = {k: v for k, v in data.items() if not isinstance(v, dict) or isinstance(v, list)}

        return data


def pivot_dict(data):
    """
    Pivots a dictionary around those with list values

    :param data: dictionary with some keys that have list values
    :return:
    """

    non_list_keys = [k for k, v in data.items() if not isinstance(v, list)]
    list_keys = [k for k, v in data.items() if isinstance(v, list)]

    out = {x: data.get(x) for x in non_list_keys}

    for key in list_keys:
        for idx, blob in enumerate(data.get(key, [])):
            if isinstance(blob, dict):
                for k, v in blob.items():
                    out.update({key + '_' + k + '_' + str(idx): v})
            else:
                out.update({key: data.get(key)})
    return out

