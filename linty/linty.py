from predikto_marshmallow import Schema, fields, ValidationError
import json, re
from typing import Union, Tuple, List, Dict
import psycopg2
from psycopg2.extras import DictCursor
from functools import reduce
from copy import deepcopy
from .checks.asset_level_key import asset_level_key

VERBOSE_CONFIG_QUERY = '''
    select f.name as service_name, f.type as service_type, p.type, p.options, p.optional, p.default, p.name
    from config_builder.functions f
    left join config_builder.function_params p
    on f.id = p.function_id
    order by service_type, service_name, name
'''


def _rows_to_verbose_configs(rows: List[Dict]):
    '''
    Takes in a array of dicts to build the verbose_configs, used when building them from SQL query
    '''
    vc = {}
    for row in rows:
        service_name = row['service_name']
        service_type = row['service_type']

        vc[service_type] = vc.get(service_type, {})
        vc[service_type][service_name] = vc[service_type].get(service_name, {})

        vc[service_type][service_name] = {**vc[service_type][service_name], row['name']: {
            k: val for k, val in row.items()
        }}

    return vc


def get_verbose_configs_wth_sql_alchemy(session):
    rows = session.execute(VERBOSE_CONFIG_QUERY).fetchall()
    return _rows_to_verbose_configs(rows)


def get_verbose_configs_with_psycopg2(host: str ='localhost', port: Union[str, int] = 5432, database: str = 'c3p0',
                                            user: str = None, password: str = None) -> Dict:
    '''

    '''
    db = {
        'host': host, 'user': user, 'password': password, 'database': database, 'port': port,
    }

    conn = psycopg2.connect(**db)
    with conn:
        with conn.cursor(cursor_factory=DictCursor) as cur:
            cur.execute(VERBOSE_CONFIG_QUERY)
            rows = cur.fetchall()
            return _rows_to_verbose_configs(rows)


def lint(config: Union[dict, str], verbose_configs: dict, allow_unknown_fields=False) -> dict:
    '''
    verbose_configs: {
        'function_type, ie service, dag_task, ...': {
            'function_name, ie esclassification': {
                'param_name': {
                    'type': 'val',
                    'options': 'json_string_val, has key of options',
                    'optional': 'boolean',
                    'default': 'json_string_val, has key of default'
                }
            }
        }
    }
    '''
    if isinstance(config, str):
        config = json.loads(config)

    result = {'errors': [], 'warnings': []}
    flow_warnings, flow_errors = lint_flow_steps(config, allow_unknown_fields)
    result['FLOW'] = {'errors': _smash_dictionaries(flow_errors), 'warnings': _smash_dictionaries(flow_warnings)}

    for name, service in config.items():
        if name.lower() == 'flow':
            continue

        service_type = service['service_type']
        verbose_service_config = verbose_configs['service'].get(service_type.lower(), None)
        result[name] = {'service_type': service_type, 'errors': [], 'warnings': []}

        warnings, errors = lint_service(service_type, service, verbose_service_config, allow_unknown_fields=allow_unknown_fields)
        result[name] = {'service_type': service_type, 'errors': errors, 'warnings': warnings}

        if service_type.lower() == 'sparky':
            dag = service.get('dag', {})
            verbose_dag_config = verbose_configs['dag_task']
            dag_result = lint_dag(dag, verbose_dag_config, allow_unknown_fields=allow_unknown_fields)
            result[name]['dag'] = dag_result

    is_okay, found = asset_level_key(config)
    if not is_okay:
        error = {'message': 'Asset Level Keys do not match for all instances of asset_level_key',
                 'value': found
                 }
        result['errors'].append({'asset_level_key': error})

    return result


def lint_service(service_type: str, service: Union[dict, str], verbose_service_configs: dict, allow_unknown_fields=False) -> (List, List):
    service_type = service_type.lower()
    if not verbose_service_configs:
        return [], [f'Service Type: {service_type} not found']

    warnings = {}
    errors = {}
    schema = _generate_service_class(service_type, verbose_service_configs)()

    non_shared_keys = {'tasks', 'task_size', 'name', 'service_type'}
    shared = {k: v for k, v in service.items() if k.lower() not in non_shared_keys}

    # Sort task so we check them in order, helps keep things in order for warnings/errors
    tasks = [{k: v} for k, v in service['TASKS'].items() if k.lower() != 'num_tasks']
    tasks = sorted(tasks, key=lambda k: int([*k.keys()][0].split('_')[1]))
    shared_warnings = {}
    shared_errors = {}
    shared_keys = [*shared.keys()]
    for task in tasks:
        for task_name, task_n in task.items():
            expanded = {**shared, **task_n}
            warning, error = _validate_with_schema(schema, expanded, unknown=allow_unknown_fields)
            # We separate shared keys from task keys to report on them differently
            # Have to check task keys first since they override shared keys, this lets us
            # report them in the TASK block if overridden vs the service block if not
            task_keys = [*task_n.keys()]
            if warning:
                task_level_warnings = {}
                for w in warning:
                    for k, v in w.items():
                        if k in task_keys:
                            task_level_warnings[k] = v
                        elif k in shared_keys:
                            shared_warnings[k] = v
                        else:
                            # some cases where it will be in neither and we need to make sure it does not get lost
                            task_level_warnings[k] = v
                if task_level_warnings:
                    warnings[task_name] = task_level_warnings
            if error:
                task_level_errors = {}
                for e in error:
                    for k, v in e.items():
                        if k in task_keys:
                            task_level_errors[k] = v
                        elif k in shared_keys:
                            shared_errors[k] = v
                        else:
                            # some cases where it is neither
                            task_level_errors[k] = v
                if task_level_errors:
                    errors[task_name] = task_level_errors
    if shared_warnings:
        for shared_key, shared_warning in shared_warnings.items():
            warnings[shared_key] = shared_warning
    if shared_errors:
        for shared_key, shared_error in shared_errors.items():
            errors[shared_key] = shared_error

    return warnings, errors


def lint_dag(dag: Union[dict, str], verbose_dag_config: dict, allow_unknown_fields=False) -> dict:
    dag_svcs = {}
    result = {}

    for dag_name, dag_service in verbose_dag_config.items():
        dag_svcs[dag_name.lower()] = _generate_service_class(dag_name.lower(), dag_service)

    for node_name, node in dag.items():
        node_type = node.get('type')
        try:
            dag_schema = dag_svcs[node_type.lower()]()
        except KeyError as e:
            result[node_name] = {
                'errors': {'type': [f'{node_type} in node {node_name} not found']},
                'warnings': {}
            }
            continue

        warning, error = _validate_with_schema(dag_schema, node.get('args', {}), unknown=allow_unknown_fields)
        if warning or error:
            result[node_name] = {
                'errors': _smash_dictionaries(error),
                'warnings': _smash_dictionaries(warning)
            }

    return result


def _validate_with_schema(schema: Schema, obj: dict, unknown=False) -> Tuple[list, list]:
    '''
    Loads the schema and captures any errors
    schema is the schema, object is the object to load
    unknown ignores extra fields if True
    '''
    errors = []
    warnings = []
    try:
        _ = schema.load(obj, unknown=unknown)
    except ValidationError as e:
        for field, validation_errors in e.messages.items():
            if 'Missing data for required field.' in validation_errors:
                warnings.append({field: validation_errors})
            else:
                errors.append({field: validation_errors})

    return warnings, errors


class IntegerStr(fields.Integer):
    """An integer field that might be a quoted string.

    :param kwargs: The same keyword arguments that :class:`Number` receives.
    """
    # override Integer
    def __init__(self, required, default, strict=False, **kwargs):
        self.strict = strict
        super().__init__(required=required, default=default, strict=strict, **kwargs)

    # override Integer
    def _format_num(self, value):
        try:
            if isinstance(value, str):
                return int(value)
            return super()._format_num(value)
        except ValueError:
            self.fail('invalid')


def _generate_param_class(param: dict):
    # type, options, optional, default
    # type: any, bool, dict, float, int, list, str, string
    _fields = {
        'any': lambda _default, _required: fields.Raw(required=_required, default=_default),
        'bool': lambda _default, _required: fields.Bool(required=_required, default=_default, strict=True),
        'dict': lambda _default, _required: fields.Dict(required=_required, default=_default),
        'float': lambda _default, _required: fields.Float(required=_required, default=_default),
        'int': lambda _default, _required: IntegerStr(required=_required, default=_default, strict=True),
        'list': lambda _default, _required: fields.List(fields.Raw, required=_required, default=_default),
        'str': lambda _default, _required: fields.Str(required=_required, default=_default),
        'string': lambda _default, _required: fields.String(required=_required, default=_default),
    }

    required = not param['optional'].lower() == 'true'
    default = json.loads(param['default'])['default']
    field = param['type'].lower()
    # TODO: Can we use options like an ENUM?
    # We can, will probably need to use @pre_load or @post_load or possibly just make our own enum field, ie extend fields.field
    # or maybe: class marshmallow.validate.OneOf(choices, labels=None, error=None)[source]
    return _fields[field](default, required)


def _generate_service_class(name: str, verbose_config: dict):
    cls = {}
    for pname, param in verbose_config.items():
        # Some functions do not have params
        if pname is None:
            continue
        param_cls = _generate_param_class(param)
        cls[pname] = param_cls

    return type(name, (Schema,), cls)



def _task_class():
    pass


def _svc_class():
    pass


def string_or_int(value: Union[str, int]) -> Union[fields.Integer, fields.String]:
    try:
        _ = int(value)
        return fields.Integer(required=True)
    except ValueError:
        return fields.String(required=True)


def _flow(flow: dict):
    '''
    num_steps: Union[str,int], step_n: string
    '''
    cls = {}
    for key, value in flow.items():
        if key == 'num_steps':
            cls[key] = string_or_int(value)
        elif key.lower().startswith('step_'):
            # Should see if we can just make a custom polymorphic field for str, [str], [str, str...]
            # The ui will always force [str...] but old configs may just be 'str'
            cls[key] = fields.List(fields.Str(), required=True)

    return type('FLOW', (Schema,), cls)


def _smash_dictionaries(list_of_dicts) -> dict:
    smashed = {}
    for a_dict in list_of_dicts:
        for key in a_dict:
            existing_values = smashed.get(key, [])
            new_values = a_dict[key]
            if isinstance(new_values, str):
                new_values = [new_values]
            existing_values.extend(new_values)
            smashed[key] = existing_values
    return smashed


def lint_flow_structure(config: Union[dict, str], allow_unknown_fields:  bool) -> dict:
    """
    Validates that the flow block contains num_fields and that each step is a string array
    :param config:
    :param allow_unknown_fields:
    :return:
    """
    config = config if isinstance(config, dict) else json.loads(config)

    if config.get('FLOW', None) is None:
        return {'warnings': {'FLOW': ['No FLOW block submitted for validation']}, 'errors': {}}

    warnings, errors = lint_flow_steps(config, allow_unknown_fields)
    result = {'warnings': _smash_dictionaries(warnings), 'errors': _smash_dictionaries(errors)}
    return result


def lint_flow_steps(config: dict, allow_unknown_fields: bool) -> (List, List):
    '''
    Returns tuple of warnings and errors
    '''
    warnings = []
    errors = []
    config = deepcopy(config)

    flow = config.pop('FLOW', None)
    if flow is None:
        error = {'FLOW': 'FLOW is required'}
        errors = [error]
        return warnings, errors

    schema = _flow(flow)()
    warnings, errors = _validate_with_schema(schema, flow, unknown=allow_unknown_fields)

    num_steps = flow.pop('num_steps', None)
    if num_steps is None:
        return warnings, errors

    try:
        num_steps = int(num_steps)
    except ValueError:
        errors = [{'num_steps': f'Value "{num_steps}" is not numeric.'}]
        return warnings, errors

    # Validate state names are formed correctly like so: step_<number>
    for step in flow:
        try:
            m = re.search(r'step_(\d*)', step)
            s = int(m.group(1))
        except Exception:
            errors.append({step: f'Step: {step} is malformed, step syntax must be in step_<number> format'})

    actual_steps = len(flow)

    # All steps in config listed in flow?  All steps listed in FLOW exist?
    services = {k for k in config.keys()}

    # steps can hold str, [str], [str, str...]
    def helper(step):
        pass
        
    step_names = set()
    service_to_step = {}
    for k, v in flow.items():
        if isinstance(v, str):
            step_names.add(v)
            service_to_step[v] = k
        elif isinstance(v, list):
            step_names.update(v)
            service_to_step.update({service: k for service in v})

    # Make sure all services listed exist
    nonconfiged_services = step_names - services
    for service in nonconfiged_services:
        errors.append({service_to_step[service]: f'Service: {service} is not in config'})

    # In services but not in FLOW
    nonstepped_services = services - step_names
    for service in nonstepped_services:
        warnings.append({service: 'Not found in FLOW'})

    if num_steps < actual_steps:
        for i in range(num_steps + 1, actual_steps + 1):
            step = f'step_{i}'
            services = flow.get(step, [])
            for service in services:
                try:
                    _ = config[service]
                    warnings.append({step: f'Service: {service} is not reached from {step}'})
                except KeyError:
                    # Missing services are handled prior to this, no need to log it here
                    pass

    if num_steps > actual_steps:
        warnings.append({'num_steps': 'Number of steps are greater than the number of actual steps provided'})

    return warnings, errors


