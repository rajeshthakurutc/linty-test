def asset_level_key(config: dict) -> (bool, dict):
    def walk(node, path=''):
        asset_level_keys = {}
        if isinstance(node, dict):
            for key, value in node.items():
                if key == 'asset_level_key':
                    asset_level_keys[path] = value
                asset_level_keys.update(walk(value, path + '.' + key if path else path + key))
        elif isinstance(node, list):
            for idx, item in enumerate(node):
                asset_level_keys.update(walk(item, path + f'[{idx}]'))

        return asset_level_keys

    asset_level_keys = walk(config)

    is_same_key = len({v for v in asset_level_keys.values()}) == 1 or len(asset_level_keys) == 0
    return is_same_key, asset_level_keys
