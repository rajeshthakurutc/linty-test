TAG := $(shell python setup.py --version)
python ?= $(shell which python3.7)
pytest ?= $(shell which pytest)
pypi ?= predikto

.PHONY: BUILD_ALL BUILD_DOCKER COMPILE_REQUIREMENTS PUBLISH_ALL \
 	PUBLISH_DOCKER PUBLISH_EGG UNIT_TEST

BUILD_ALL: BUILD_DOCKER

BUILD_DOCKER:	
	docker build -t predikto-linty .
	docker tag predikto-linty 425470256342.dkr.ecr.us-east-1.amazonaws.com/predikto-linty:$(TAG)

PUBLISH_ALL: PUBLISH_DOCKER PUBLISH_EGG

PUBLISH_DOCKER:
	docker push 425470256342.dkr.ecr.us-east-1.amazonaws.com/predikto-linty:$(TAG)

BUILD_EGG:
	$(python) setup.py sdist

PUBLISH_EGG:
	twine upload --repository  $(pypi)  dist/*

UNIT_TEST:
	$(pytest)

