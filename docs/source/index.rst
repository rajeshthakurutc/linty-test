.. linty documentation master file, created by
   sphinx-quickstart on Fri Jul 22 10:53:10 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Predikto Linty
==============

A small utility for doing static analysis of configuration files.

Basic usage:

.. code-block:: python

   from linty import ConfigurationFile
   cf = ConfigurationFile(some_config_file)
   flow = cf.get_flow()
   unknowns = cf.get_unknown_blocks(raise_exceptions=True)

Contents:

.. toctree::
   :maxdepth: 2

   ConfigurationFile



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

