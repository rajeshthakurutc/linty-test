import json

from linty import lint_flow_structure


def test_no_flow():
    test_config = json.loads("""{}""")
    result = lint_flow_structure(test_config, False)
    assert result['errors'] == {}
    assert 'No FLOW block submitted for validation' in result['warnings']['FLOW']


def test_lint_flow_empty():
    test_config = json.loads("""{
        "FLOW": {}
    }""")
    result = lint_flow_structure(test_config, False)
    assert result['errors'] == {}
    assert result['warnings'] == {}


def test_lint_non_numeric_step_count():
    test_config = json.loads("""{
        "FLOW": {
            "num_steps": "X"
        }
    }""")
    result = lint_flow_structure(test_config, False)
    assert result['errors']['num_steps'] == ['Value "X" is not numeric.']
    assert result['warnings'] == {}


def test_lint_numeric_step_count():
    test_config = json.loads("""{
        "FLOW": {
            "num_steps": "2"
        }
    }""")
    result = lint_flow_structure(test_config, False)
    assert result['errors'] == {}
    assert 'Number of steps are greater than the number of actual steps provided' in result['warnings']['num_steps']


def test_bad_step_format():
    test_config = json.loads("""{
        "FLOW": {
            "num_steps": 1,
            "step_1": "not_a_list",
            "step_2": {},
            "step_3": {"this":"should fail"}
        }
    }""")
    result = lint_flow_structure(test_config, False)
    assert 'Not a valid list.' in result['errors']['step_1']
    assert 'Not a valid list.' in result['errors']['step_2']
    assert 'Not a valid list.' in result['errors']['step_3']
    assert result['warnings'] == {}


def test_good_step_format():
    test_config = json.loads("""{
        "FLOW": {
            "num_steps": 1,
            "step_1": ["item_1","item_2"]
        }
    }""")
    result = lint_flow_structure(test_config, False)
    assert 'Not a valid list.' not in result['errors']['step_1']
    assert result['warnings'] == {}


def test_bad_step_count():
    test_config = json.loads("""{
        "FLOW": {
            "num_steps": 2,
            "step_1": ["service_1"]
        }
    }""")
    result = lint_flow_structure(test_config, False)
    assert 'Not a valid list.' not in result['errors']['step_1']
    assert 'Number of steps are greater than the number of actual steps provided' in result['warnings']['num_steps']


def test_with_a_service_def():
    test_config = json.loads("""{
        "FLOW": {
            "num_steps": 1,
            "step_1": ["service_1"]
        },
        "service_1": {
        }
    }""")
    result = lint_flow_structure(test_config, False)
    assert result['errors'] == {}
    assert result['warnings'] == {}


def test_with_a_bad_service():
    test_config = json.loads("""{
        "FLOW": {
            "num_steps": 1,
            "step_1": ["service_1"]
        },
        "service_2": {
        }
    }""")
    result = lint_flow_structure(test_config, False)
    assert 'Service: service_1 is not in config' in result['errors']['step_1']
    assert 'Not found in FLOW' in result['warnings']['service_2']


def test_with_bigger_config():
    test_config = json.loads("""{
        "FLOW": {
            "num_steps": 1,
            "step_1": ["THE_STEP"]
        },
        "THE_STEP": {
            "service_type": "some_service",
            "name": "the_service_step"
        }
    }""")
    result = lint_flow_structure(test_config, False)
    assert result['errors'] == {}
    assert result['warnings'] == {}


def test_with_skip_step_numbers():
    # Using num_steps = 1 to force this test through a particular block in linty code
    test_config = json.loads("""{
        "FLOW": {
            "num_steps": 1,
            "step_1": ["service_1"],
            "step_5": ["service_2"],
            "step_7": ["service_3"]
        }
    }""")
    result = lint_flow_structure(test_config, False)
    assert 'Service: service_1 is not in config' in result['errors']['step_1']
    assert 'Service: service_2 is not in config' in result['errors']['step_5']
    assert 'Service: service_3 is not in config' in result['errors']['step_7']
    assert result['warnings'] == {}

def test_malformed_step():
    test_config = json.loads("""{
        "FLOW": {
            "num_steps": 3,
            "step_1": ["DO_SOMETHING"],
            "step_100": ["DO_SOMETHING"],
            "step_x": ["NOW_LINTY_DIES"]
        }, 
        "DO_SOMETHING": {
            "service_type": "some_service",
            "name": "the_service_step"
        },
        "NOW_LINTY_DIES": {
            "service_type": "some_service",
            "name": "the_service_step"
        }
    }""")
    result = lint_flow_structure(test_config, False)
    assert 'Step: step_x is malformed, step syntax must be in step_<number> format' in result['errors']['step_x']