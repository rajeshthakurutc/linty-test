import json

from linty import lint

REQ_STR = {'type': 'str', 'options': '{"options": null}', 'optional': 'false', 'default': '{"default": null}'}
REQ_INT = {'type': 'int', 'options': '{"options": null}', 'optional': 'false', 'default': '{"default": null}'}
DAG = {'type': 'dict', 'options': '{"options": null}', 'optional': 'false', 'default': '{"default": null}'}


def test_lint_dag():
    verbose_config = {
        "service": {
            "sparky": {
                "name": REQ_STR,
                "task_size": REQ_INT,
                "task_timeout": REQ_INT,
                "dag": DAG
            }
        },
        "dag_task": {
            "dag_type_2": {
                "int_arg": REQ_INT
            }
        }
    }
    test_config = json.loads("""{
        "FLOW": {
            "num_steps": "1",
            "step_1": ["SPARKY_STEP"]
        },
        "SPARKY_STEP": {
            "service_type": "sparky",
            "TASKS": {
                "TASK_1": {
                    "name": "task_1",
                    "task_size": "0",
                    "task_timeout": "7200"
                },
                "num_tasks": "1"
            },
            "dag": {
                "000_dag_0": {
                    "after": "None",
                    "args": {
                        "string_arg": "Valid String",
                        "int_arg": 1
                    },
                    "type": "dag_type_1"
                },
                "001_dag_1": {
                    "after": "999_does_not_exist",
                    "args": {},
                    "type": "dag_type_2"
                },
                "002_dag_2": {
                    "after": "000_dag_0",
                    "args": {
                        "int_arg": 12
                    },
                    "type": "dag_type_2"
                }
            }       
        }
    }""")

    result = lint(test_config, verbose_config, allow_unknown_fields=False)
    assert 'dag_type_1 in node 000_dag_0 not found' in result['SPARKY_STEP']['dag']['000_dag_0']['errors']['type']
    assert 'Missing data for required field.' in result['SPARKY_STEP']['dag']['001_dag_1']['warnings']['int_arg']
    assert '002_dag_2' not in result['SPARKY_STEP']['dag'].keys()
