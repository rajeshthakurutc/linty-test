import json

from linty import lint_service


def test_lint_service_missing_required():
    verbose_service_config = \
        {
            'name': {'type': 'str', 'options': '{"options": null}', 'optional': 'false',
                     'default': '{"default": null}'},
            'task_timeout': {'type': 'int', 'options': '{"options": null}', 'optional': 'false',
                             'default': '{"default": null}'}
        }
    service = json.loads("""{
        "TASKS": {
            "TASK_2": {
                "name": "Missing the task_timeout field"
            }
        }
    }""")

    warnings, errors = lint_service('metadata', service, verbose_service_config, allow_unknown_fields=False)
    assert 'Missing data for required field.' in warnings["TASK_2"]["task_timeout"]


def test_lint_service_optional_fields():
    verbose_service_config = \
        {
            'name': {'type': 'str', 'options': '{"options": null}', 'optional': 'false',
                     'default': '{"default": null}'},
            'some_optional': {'type': 'str', 'options': '{"options": null}',
                              'optional': 'true', 'default': '{"default": null}'}
        }
    service = json.loads("""{
        "TASKS": {
            "TASK_1": {
                "name": "No value for optional field"
            },
            "TASK_2" : {
                "name": "Value for optional field",
                "some_optional": "This is optional"
            },
            "TASK_3" : {
                "name": "Bad value for optional field",
                "some_optional": 12
            }
        }
    }""")

    warnings, errors = lint_service('metadata', service, verbose_service_config, allow_unknown_fields=False)
    assert len(warnings) == 0
    assert "Not a valid string." in errors["TASK_3"]["some_optional"]
    # Make sure optional field worked fine in our other two tasks
    assert "TASK_1" not in errors.keys()
    assert "TASK_2" not in errors.keys()

