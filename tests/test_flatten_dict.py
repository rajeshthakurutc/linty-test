import unittest

import sys

from linty.common import flatten_dict

__author__ = 'devgupta'


class TestFlattenDict(unittest.TestCase):

    def test_flatten_dict(self):
        to_flatten = {
            'SPARKY': {
                'SOME_TASK': {
                    'FOO': 'BAR'
                },
                'foobar': 1
            }
        }
        expected = {'SPARKY.SOME_TASK.FOO': 'BAR', 'SPARKY.foobar': 1}
        actual = flatten_dict(to_flatten, layers=sys.getrecursionlimit(), drop_deeper=False)
        self.assertDictEqual(expected, actual)