import json

from linty import lint
from linty.common import flatten_dict


def flatten(l) -> list:
    return sum(map(flatten, l), []) if isinstance(l, list) else [l]


def no_warnings(r):
    flat_r = flatten_dict(r)
    return len(flatten([flat_r[k] for k in flat_r.keys() if "warnings" in k])) == 0


def no_errors(r):
    flat_r = flatten_dict(r)
    return len(flatten([flat_r[k] for k in flat_r.keys() if "errors" in k])) == 0


def test_lint_empty():
    test_config = json.loads("{}")
    result = lint(test_config, {})
    expected = {"FLOW": ["FLOW is required"]}
    assert result["FLOW"]["errors"] == expected
    assert result["FLOW"]["warnings"] == {}


def test_lint_flow_empty():
    test_config = json.loads("""{
        "FLOW": {}
    }""")
    result = lint(test_config, {})
    assert result["FLOW"]["errors"] == {}
    assert result["FLOW"]["warnings"] == {}


def test_lint_flow_bad_step_count():
    test_config = json.loads("""{
        "FLOW": {
            "num_steps": 20
        }
    }""")
    result = lint(test_config, {})
    assert 'Number of steps are greater than the number of actual steps provided' in result["FLOW"]["warnings"]["num_steps"]
    assert result["FLOW"]["errors"] == {}


def test_lint_flow_steps():
    test_config = json.loads("""{
        "FLOW": {
            "num_steps": 0,
            "step_1": "ERROR_STEP"
        }
    }""")
    result = lint(test_config, {})
    flow_errors = result["FLOW"]["errors"]['step_1']
    expected = ['Not a valid list.', 'Service: ERROR_STEP is not in config']
    assert sorted(flow_errors) == sorted(expected)


def test_lint_dual_flow():
    test_config = json.loads("""{
        "FLOW": {
            "num_steps": -1,
            "step_1": ["TEST_STEP_FLOW_1"]
        },
        "FLOW": {
            "num_steps": 1,
            "step_1": ["TEST_STEP_FLOW_2"]
        },
        "TEST_STEP_FLOW_2": {
            "TASKS": {},
            "name": "some_name",
            "service_type": "some_service",
            "bob": "not good"
        }
    }""")
    result = lint(test_config, {'service': {'some_service': {'some_int': {'type': 'int', 'options': '{"options": null}',
                                                                          'optional': 'true',
                                                                          'default': '{"default": null}'}}}})
    assert no_errors(result)
    assert no_warnings(result)


def test_lint_type_checks_int():
    test_config = json.loads("""{
    "FLOW": {
        "num_steps": 1,
        "step_1": ["TEST_STEP_FLOW"]
    },
    "TEST_STEP_FLOW": {
        "service_type": "some_service",
        "TASKS":  {
            "TASK_1": {
                "some_int": 1.2
            },
            "TASK_2": {
                "some_int": "1.2"
            },
            "TASK_3": {
                "some_int": 1
            },
            "TASK_4": {
                "some_int": "1"
            },
            "TASK_5": {
                "some_int": "one"
            }
        }
    }
    }""")
    result = lint(test_config, {'service': {'some_service': {'some_int': {'type': 'int', 'options': '{"options": null}',
                                                                          'optional': 'true',
                                                                          'default': '{"default": null}'}}}})

    err_array = result["TEST_STEP_FLOW"]["errors"]
    assert 'Not a valid integer.' in err_array['TASK_1']['some_int']
    assert 'Not a valid integer.' in err_array['TASK_2']['some_int']
    assert 'Not a valid integer.' in err_array['TASK_5']['some_int']


def test_lint_invalid_service():
    test_config = json.loads("""{
        "FLOW": {
            "num_steps": 2,
            "step_1": ["THE_STEP"]
        },
        "THE_STEP": {
            "service_type": "some_service",
            "name": "the_service_step"
        }
    }""")
    result = lint(test_config, {"service": {}})
    expected = ['Service Type: some_service not found']
    service_error = result["THE_STEP"]["errors"]
    assert service_error == expected


def test_lint_type_checks_shared():
    test_config = json.loads("""{
        "FLOW": {
            "num_steps": 1,
            "step_1": ["TEST_STEP_FLOW"]
        },
        "TEST_STEP_FLOW": {
            "service_type": "some_service",
            "some_int": 1.2,
            "TASKS":  {
                "TASK_1": {},
                "TASK_2": {},
                "TASK_3": {},
                "TASK_4": {
                    "some_int": "NaN"
                },
                "TASK_5": {
                    "some_int": 1
                }
            }
        }
    }""")
    result = lint(test_config, {'service': {'some_service': {'some_int': {'type': 'int', 'options': '{"options": null}',
                                                                          'optional': 'true',
                                                                          'default': '{"default": null}'}}}})
    assert 'Not a valid integer.' in result['TEST_STEP_FLOW']['errors']['some_int']
    assert 'Not a valid integer.' in result['TEST_STEP_FLOW']['errors']['TASK_4']['some_int']
    # Do it differently with the shared field being correct
    test_config = json.loads("""{
        "FLOW": {
            "num_steps": 1,
            "step_1": ["TEST_STEP_FLOW"]
        },
        "TEST_STEP_FLOW": {
            "service_type": "some_service",
            "some_int": 1,
            "TASKS":  {
                "TASK_1": {},
                "TASK_2": {},
                "TASK_3": {},
                "TASK_4": {
                    "some_int": "NaN"
                },
                "TASK_5": {
                    "some_int": 1.2
                }
            }
        }
    }""")
    result = lint(test_config, {'service': {'some_service': {'some_int': {'type': 'int', 'options': '{"options": null}',
                                                                          'optional': 'true',
                                                                          'default': '{"default": null}'}}}})
    assert 'Not a valid integer.' in result['TEST_STEP_FLOW']['errors']['TASK_4']['some_int']
    assert 'Not a valid integer.' in result['TEST_STEP_FLOW']['errors']['TASK_5']['some_int']


def test_lint_bool():
    test_config = json.loads("""
      {
        "FLOW": {
          "num_steps": 1,
          "step_1": ["the_1_step"]
        },
        "the_1_step": {
          "service_type": "service_a",
          "name": "the_1_step_name",
          "TASKS": {
            "TASK_1": {
                "some_bool": true
            },
            "TASK_2": {
                "some_bool": false
            },
            "TASK_3": {
                "some_bool": 1
            },
            "TASK_4": {
                "some_bool": -1
            },
            "TASK_5": {
                "some_bool": []
            },
            "TASK_6": {
                "some_bool": "true"
            },
            "TASK_7": {
                "some_bool": "false"
            },
            "TASK_8": {
                "some_bool": 0
            }                                  
          }
        }
      }
    """)
    result = lint(test_config,
                  {'service': {'service_a': {'some_bool': {'type': 'bool', 'options': '{"options": null}',
                                                           'optional': 'true', 'default': '{"default": null}'}}}})
    assert "TASK_1" not in result["the_1_step"]["errors"].keys()
    assert "TASK_2" not in result["the_1_step"]["errors"].keys()
    assert "TASK_3" not in result["the_1_step"]["errors"].keys()
    assert "Not a valid boolean." in result["the_1_step"]["errors"]["TASK_4"]["some_bool"]
    assert "Not a valid boolean." in result["the_1_step"]["errors"]["TASK_5"]["some_bool"]
    assert "TASK_6" not in result["the_1_step"]["errors"].keys()
    assert "TASK_7" not in result["the_1_step"]["errors"].keys()
    assert "TASK_8" not in result["the_1_step"]["errors"].keys()


def test_asset_level_keys():
    verbose_config = {'service': {'some_service':
                                  {'some_int': {'type': 'int', 'options': '{"options": null}',
                                                'optional': 'false', 'default': '{"default": null}'},
                                   'asset_level_key': {'type': 'str', 'options': '{"options": null}',
                                                       'optional': 'false', 'default': '{"default": null}'}}}}
    test_config = json.loads("""{
        "FLOW": {
            "num_steps": 3,
            "step_1": ["STEP_1"],
            "step_2": ["STEP_2"],
            "step_3": ["STEP_3"]
        },
        "STEP_1": {
            "service_type": "some_service",
            "some_int": 1,
            "asset_level_key": "correct_value",
            "TASKS":  {
                "TASK_1": {}
            }
        },
        "STEP_2": {
            "service_type": "some_service",
            "some_int": 2,
            "asset_level_key": "correct_value",
            "TASKS":  {
                "TASK_1": {}
            }
        },
        "STEP_3": {
            "service_type": "some_service",
            "some_int": 3,
            "asset_level_key": "bad_value",
            "TASKS":  {
                "TASK_1": {}
            }
        }                
    }""")

    result = lint(test_config, verbose_config, allow_unknown_fields=False)
    assert 'asset_level_key' in result["errors"][0]

# def test_lint_dag():
#     verbose_dag_config = {'pull_file_from_s3': VC['dag_task']['pull_file_from_s3']}
#     dag = {
#         "000_attribute_load_file_event55555": {
#             "after": "None",
#             "args": {
#                 "idontexist": "value",
#                 "base_name": "attributes_*",
#                 "count": 2,
#                 "data_source": {
#                     "aws_access_key": "vault:AWS.ACCESS_KEY",
#                     "aws_secret_key": "vault:AWS.SECRET_KEY",
#                     "host_name": "s3.amazonaws.com",
#                     "location": "s3",
#                     "path": "s3n://predikto-jmi-locomotives/"
#                 },
#                 "ls_prefix": None,
#                 "method": "rowwise",
#                 "mode": "oldest",
#                 "num_partitions": 64,
#                 "small_files": False,
#                 "target_db": {
#                     "database": "JMI",
#                     "file_processing_log": "spark_processing_log",
#                     "host_name": "etl-test.ckyw1pwlibsb.us-east-1.rds.amazonaws.com",
#                     "password": "big$exit!1",
#                     "schema": "locomotives",
#                     "table_name": "agg_table",
#                     "username": "predikto"
#                 },
#                 "use_unicode": True
#             },
#             "type": "pull_file_from_s3"
#         }
#     }
#     result = lint_dag(dag, verbose_dag_config, allow_unknown_fields=False)
#     result = result['000_attribute_load_file_event55555']
#
#     assert 1 == len(result['errors'])
#     assert 1 == len(result['warnings'])
#
# def test_lint_disabled():
#     config = {
#         "FLOW": {
#             "num_steps": 0,
#             "step_1": ["asdf"],
#         },
#         "asdf": {
#             "TASKS": {
#                     "TASK_1": {
#                             "asset_level_key": "device_id",
#                             "name": "blahljj"
#                     },
#                     "num_tasks": 1
#             },
#             "data_table": "foo",
#             "database": "bar",
#             "dbtype": "foobar",
#             "device_id_regex_filter": "foo",
#             "epoch_name": "name",
#             "es_data_type": "dst",
#             "es_hostname": "blah",
#             "es_index": "blah",
#             "es_port": "blah",
#             "hostname": "blah",
#             "models_index": "blah",
#             "output_description": "asdf",
#             "output_table_name": "asdf",
#             "password": "asdf",
#             "save_to_db": False,
#             "schema": "ss",
#             "service_region": "US",
#             "service_type": "classification",
#             "tier_name": "foowho",
#             "username": "ddd"
#         }
#     }
#     result = lint(config, VC, allow_unknown_fields=False)
#     assert 0 < len(result['FLOW']['warnings'])
