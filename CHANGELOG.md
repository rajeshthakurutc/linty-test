v1.1.11
=======
  * Reduction in installation dependencies

v1.1.8
======
  * C3P0-647
  * Required Field Validation messages no longer lost
  * Removal of dead code
  * Clean up of unit tests

v1.1.7
======
  * C3P0-544
  * C3P0-596
  * C3P0-599
  * C3P0-643
  * Removal of dead code
  * Clean up of unit tests

v1.1.6
======
  * Added method `lint_flow_structure` to perform a high-level validation of the FLOW block structure

v1.1.5
======
  * Linty should quit complaining about the `service_type` field 
  * Fix last release

v1.1.4
======
  * Unit tests should not depend on directory from which `pytest` is launched

v1.1.3
======

v1.1.2
======

v1.1.1
======
  * Rewrite

v0.2.3
======

 * Linty now supports warnings in `find_issues`

v0.2.2
======

 * deprecated subtasks
 
v0.2.1
======

 * vault support
 
v0.2.0
======

 * CDB instead of a giant if-else
 
v0.1.9
======

 * Added featureselectionpipe
 
v0.1.8
======
 
 * Added the threshold optimizer

v0.1.7
======

 * Added reindexer
 
v0.1.6
======

 * service names update
 
v0.1.5
======

 * Extractor removed
 * Added test model
 
v0.1.4
======

 * Added mover service
 * Removed copier, summarizer, archiver and forwarder services
 
v0.1.3
======

 * more bugfixes
 
v0.1.2
======
 
 * bugfixes
 
v0.1.1
======

 * Started checking the types of fields, more strictly than runtime checking
 * For type dict, only valid nested or stringified json will pass linting.
 
v0.1.0
======

 * Deperecated name convention based service typing
 * Explicit service typing now required.
 
v0.0.9
======

 * Switched testing backend to nose2
 * Added metadata service
 
v0.0.8
======

 * Bug fixes (Deep copy of the config so it does not mutate, and pointing to the correct child on CDB)

v0.0.7
======
 
 * Key/Value config pairs are now validated
 * Feature Selection and Validator are now valid services
 
v0.0.4
======

 * raise error on unparsable service
 
v0.0.3
======

 * Optional central db interface to check parameter configs.

v0.0.2
======

 * More depth, to the subtask level, more sophisticated checks of missing
 required fields at all levels.

v0.0.1
======

 * Initial commits, created a ConfigurationFile object for doing basic 
 static analysis of config files, starting with flow-based checks.
 
